﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;
using Atlassian.Jira;
using CredentialManagement;

namespace JiraBugReview
{
    class IssueLister
    {
        public async Task Start()
        {

            string jiraUrl = "https://jira.forge.avaya.com/";

            var credential = new Credential
            {
                Target = jiraUrl
            };

            bool areCredentialsValid;
            do
            {
                if (!credential.Load())
                {
                    Console.Write("Enter JIRA username: "); var username = Console.ReadLine();
                    Console.Write("Enter JIRA password: "); var password = Helpers.GetConsolePassword();

                    credential = new Credential(username, password)
                    {
                        Target = jiraUrl
                    };

                    credential.Save();
                }

                Console.WriteLine("Checking if credentials are valid...");
                areCredentialsValid = await AreCredentialsValid(jiraUrl, credential.Username, credential.Password);

                if (!areCredentialsValid)
                {
                    Console.WriteLine("Credentials invalid...");
                    credential.Delete();
                }

            } while (!areCredentialsValid);
           

            var jira = Jira.CreateRestClient(jiraUrl,
                credential.Username,
                credential.Password);

            Console.WriteLine("Fetching bugs from JIRA");

            var queryForMyOpenBugs = "assignee = currentUser() AND resolution = Unresolved AND type = BUG order by updated DESC";
            var queryForBugsResolvedByMe = "status was Resolved by currentUser() AND resolutiondate >= -3d";


            var myOpenBugs = await jira.Issues.GetIssuesFromJqlAsync(queryForMyOpenBugs, 1000);


            var resolvedByMeInLastDays = await jira.Issues.GetIssuesFromJqlAsync(queryForBugsResolvedByMe, 10000);

            using (var writer = new StreamWriter("bugs_review.html"))
            {
                ReviewBugs(writer, myOpenBugs.ToList(), resolvedByMeInLastDays.ToList());
            }
            Console.WriteLine("Done, opening bugs_review.html");
            Process.Start("bugs_review.html");
            
            Console.ReadLine();
        }

        private async Task<bool> AreCredentialsValid(string jiraUrl, string username, string password)
        {
            try
            {
                var testClient = Jira.CreateRestClient(jiraUrl, username, password);
                var customFields = await testClient.Fields.GetCustomFieldsAsync();
                return true;
            }
            catch (AuthenticationException)
            {
                return false;
            }
            
        }

        private void WriteBugSection(StreamWriter wr, string sectionTitle, IEnumerable<Issue> issues)
        {
            wr.WriteSectionTitle(sectionTitle);

            foreach (var bug in issues.OrderBy(s => s.Priority.Id))
            {
                Helpers.WriteBug(wr, bug);
            }
        }

        private void ReviewBugs(StreamWriter wr, List<Issue> myOpenBugs, List<Issue> resolvedBugs)
        {
            wr.WriteHtmlDocumentStart();

            wr.WriteLineBreak();
            wr.WriteLine($"Total issues: {myOpenBugs.Count}");
            wr.WriteLineBreak();

            var pcsdkBugs = myOpenBugs.Where(i => i.Labels.Contains("Desktop_PCSDK")).ToList();

            var sdkBugs = myOpenBugs.Where(i => i.Labels.Contains("Desktop_CSDK")).ToList();

            var pcsdkAndSdkBugs = new  List<Issue>();
            pcsdkAndSdkBugs.AddRange(pcsdkBugs);
            pcsdkAndSdkBugs.AddRange(sdkBugs);

            var notCategorizedBugs =
                myOpenBugs.Where(
                    i =>
                        !i.Labels.Contains("Desktop_PCSDK") && 
                        !i.Labels.Contains("Desktop_CSDK") &&
                        !i.Labels.Contains("Desktop_UI"));

            var bugsGrouppedByComponent = myOpenBugs
                .Where(s => !pcsdkAndSdkBugs.Contains(s))
                .ToList()
                .GroupBy(c =>
                {
                    var firstOrDefault = c.Components.FirstOrDefault();
                    return firstOrDefault?.Name;
                })
                .ToList();

            WriteBugSection(wr, "Potential CSDK Bugs", pcsdkBugs);

            WriteBugSection(wr, "SDK Bugs", sdkBugs);

            WriteBugSection(wr, "Resolved By Me", resolvedBugs);

            WriteBugSection(wr, "Not categorized", notCategorizedBugs);
            

            foreach (var bugGroup in bugsGrouppedByComponent)
            {
                var sectionTitle = bugGroup.Key ?? "No component";
                WriteBugSection(wr, sectionTitle, bugGroup);
            }

            wr.WriteHtmlDocumentEnd();
        }
        
    }
}
