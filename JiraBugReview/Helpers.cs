﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Atlassian.Jira;

namespace JiraBugReview
{
    public static class Helpers
    {

        private static string GetSeverity(Issue issue)
        {
            var customFiled = issue.CustomFields["Severity"];
            if (customFiled == null)
            {
                return null;
            }
            var fieldValue = customFiled.Values.FirstOrDefault();
            if (fieldValue == null)
            {
                return "S[null]";
            }

            return "S" + fieldValue.Substring(0, 1);
        }
        public static void WriteBug(StreamWriter wr, Issue issue)
        {
            var link = GenerateLink("https://jira.forge.avaya.com/browse/" + issue.Key, issue.Key.ToString());
            var twoLetterPriority = issue.Priority.Name.Substring(0, 2);

            var twoLetterSeverity = GetSeverity(issue);

            var prioritySeverity = $"{twoLetterPriority}:{twoLetterSeverity}";

            wr.WriteLine($"\t{link} [<b>{prioritySeverity}</b>] {issue.Summary} </br>");
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string GenerateLink(string link, string text)
        {
            return $"<a style=\"margin-left:5px\" target=\"_blank\" href=\"{link}\">{text}</a>";
        }

        public static string GetConsolePassword()
        {
            var sb = new StringBuilder();
            while (true)
            {
                var cki = Console.ReadKey(true);
                if (cki.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    break;
                }

                if (cki.Key == ConsoleKey.Backspace)
                {
                    if (sb.Length > 0)
                    {
                        Console.Write("\b\0\b");
                        sb.Length--;
                    }

                    continue;
                }

                Console.Write('*');
                sb.Append(cki.KeyChar);
            }

            return sb.ToString();
        }
    }
}
