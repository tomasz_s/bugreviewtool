﻿using System.IO;

namespace JiraBugReview
{
    public static class WriterExtensions
    {
        public static void WriteLineBreak(this StreamWriter wr)
        {
            wr.Write("<br/>");
        }

        public static void WriteSectionTitle(this StreamWriter wr, string title)
        {
            wr.WriteLineBreak();
            wr.WriteLine("{0}:", title);
            wr.WriteLineBreak();
        }

        public static void WriteSectionTitle(this StreamWriter wr, string title, string titleWhenNull)
        {
            wr.WriteLineBreak();
            if (title == null)
            {
                title = titleWhenNull;
            }
            wr.WriteLine("{0}:", title);
            wr.WriteLineBreak();
        }

        public static void WriteHtmlDocumentStart(this StreamWriter wr)
        {
            wr.WriteLine(@"<head>
	            <style>
	            html 
	            { 
		            font-family: Calibri;
		            font-size: 15;
	            }
	            </style>
            </head>

            <body>");
        }

        public static void WriteHtmlDocumentEnd(this StreamWriter wr)
        {
            wr.WriteLine("</body>");
        }
    }
}
