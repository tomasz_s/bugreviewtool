﻿using System.Net;
using System.Threading.Tasks;

namespace JiraBugReview
{
    static class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).GetAwaiter().GetResult();
        }

        public static async Task MainAsync(string[] args)
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;
            var lister = new IssueLister();
            await lister.Start();
        }
    }
}
